import 'package:http/http.dart' as http;
import 'dart:convert';

class UserGet {
  String id, firstName, email, avatar;

  UserGet({this.id, this.firstName, this.email, this.avatar});

  static Future<UserGet> connectToApiUser(String id) async {
    String apiURLPOST = 'http://reqres.in/api/users/' + id;

    // Get data masih mentah
    var apiResult = await http.get(apiURLPOST);

    // Data dijadikan json
    var jsonObject = json.decode(apiResult.body);

    // Mapping data
    var userData = (jsonObject as Map<String, dynamic>)['data'];

    return UserGet.createUserGet(userData);
  }

  factory UserGet.createUserGet(Map<String, dynamic> object) {
    return UserGet(
        id: object['id'].toString(),
        firstName: object['first_name'],
        email: object['email'],
        avatar: object['avatar']);
  }
}
